// Package version prints version information.
package version

import (
	"fmt"
	"io"
	"os"
	"runtime"
	"runtime/debug"
)

// Version is set at compile time with:
//    -ldflags "-X 'gitlab.com/boldlygo/version.Version=x.y.z'"
//nolint:gochecknoglobals // needs to be setable at compile time
var Version = "(devel)"

func printModuleInfo(w io.Writer, m *debug.Module) {
	fmt.Fprintf(w, "    %s@%s", m.Path, m.Version)

	if m.Sum != "" {
		fmt.Fprintf(w, " %s", m.Sum)
	}

	if m.Replace != nil {
		fmt.Fprintf(w, " => %s", m.Replace.Path)
	}

	fmt.Fprintf(w, "\n")
}

// Print writes version information to w.
func Print(w io.Writer) {
	info, ok := debug.ReadBuildInfo()

	if ok {
		fmt.Fprintf(w, "%s", info.Path)
	} else {
		fmt.Fprintf(w, "%s", os.Args[0])
	}

	fmt.Fprintf(w, " %s (%s %s/%s)\n", Version, runtime.Version(), runtime.GOOS, runtime.GOARCH)

	if ok {
		printModuleInfo(w, &info.Main)
	} else {
		fmt.Fprintf(w, "    built in $GOPATH mode")
	}

	fmt.Fprintf(w, "\n")

	if ok {
		for _, dep := range info.Deps {
			printModuleInfo(w, dep)
		}
	}
}
