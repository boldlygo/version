package main

import (
	"os"

	"gitlab.com/boldlygo/version"
)

func main() {
	version.Print(os.Stdout)
}
