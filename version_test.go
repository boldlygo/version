package version //nolint:testpackage // performs internal tests

import (
	"bytes"
	"fmt"
	"os/exec"
	"runtime"
	"runtime/debug"
	"testing"

	"github.com/google/go-cmp/cmp"
)

//nolint:lll // tests
func TestPrint(t *testing.T) {
	type args struct {
		version string
	}

	tests := []struct {
		name string
		args args
		want string
	}{
		{
			"",
			args{"x.y.z"},
			fmt.Sprintf("gitlab.com/boldlygo/version/example x.y.z (%s %s/%s)\n    gitlab.com/boldlygo/version@(devel)\n", runtime.Version(), runtime.GOOS, runtime.GOARCH),
		},
	}
	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			cmd := exec.Command("go", "run", "-ldflags", fmt.Sprintf("-X 'gitlab.com/boldlygo/version.Version=%s'", tt.args.version), "./example/...")
			got, err := cmd.CombinedOutput()
			if err != nil {
				t.Error(err)
			}
			if diff := cmp.Diff(tt.want+"\n", string(got)); diff != "" {
				t.Errorf("printModuleInfo() output mismatch (-want +got):\n%s", diff)
			}
		})
	}
}

func Test_printModuleInfo(t *testing.T) {
	type args struct {
		m *debug.Module
	}

	tests := []struct {
		name string
		args args
		want string
	}{
		{
			"",
			args{&debug.Module{
				Path:    "path",
				Version: "version",
			}},
			"    path@version\n",
		},
		{
			"",
			args{&debug.Module{
				Path:    "path",
				Version: "version",
				Sum:     "sum",
			}},
			"    path@version sum\n",
		},
		{
			"",
			args{&debug.Module{
				Path:    "path",
				Version: "version",
				Sum:     "sum",
				Replace: &debug.Module{Path: "replace"},
			}},
			"    path@version sum => replace\n",
		},
	}
	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			w := &bytes.Buffer{}
			printModuleInfo(w, tt.args.m)
			if diff := cmp.Diff(tt.want, w.String()); diff != "" {
				t.Errorf("printModuleInfo() output mismatch (-want +got):\n%s", diff)
			}
		})
	}
}
